import './App.css';
import { Route, Switch, Redirect } from "react-router-dom";
import { Homepage } from "./pages/HomePage/Homepage";
import { ShopPage } from "./pages/ShopPage/ShopPage";
import { Header } from "./components/Header/Header";
import { SignInPage } from "./pages/SignInPage/SignInPage";
import { auth, createUserProfileDocument } from "./firebase/firebase";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setCurrentUser } from "./redux/user/userActions";
import { selectCurrentUser } from "./redux/user/userSelectors";
import { CheckoutPage } from "./pages/CheckoutPage/CheckoutPage";

function App() {
    const currentUser = useSelector(selectCurrentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(async (userAuth) => {
            if (userAuth) {
                const userRef = await createUserProfileDocument(userAuth);
                userRef.onSnapshot(snapshot => dispatch(setCurrentUser({
                    id: snapshot.id,
                    ...snapshot.data(),
                })));
            }
            dispatch(setCurrentUser(userAuth));
        });
        return () => {
            unsubscribe();
        }
    }, []);

    return (
        <div className="App">
            <Header />
            <Switch>
                <Route exact path={"/"} component={Homepage} />
                <Route path={"/shop"} component={ShopPage} />
                <Route exact path={"/signin"} render={() => currentUser ? <Redirect to={"/"} /> : <SignInPage />} />
                <Route exact path={"/checkout"} component={CheckoutPage} />
            </Switch>
        </div>
    );
}

export default App;
