import React from "react";
import "./SignInPage.styles.scss";
import { SignIn } from "../../components/SignIn/SignIn";
import { SignUp } from "../../components/SignUp/SignUp";

export const SignInPage = () => {
    return (
        <div className={"sign-in-page"}>
            <SignIn />
            <SignUp />
        </div>
    );
}