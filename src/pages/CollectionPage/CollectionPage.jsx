import React from "react";
import "./CollectionPage.styles.scss";
import { CollectionItem } from "../../components/CollectionItem/CollectionItem";
import { useSelector } from "react-redux";
import { selectShopCollection } from "../../static/shopSelectors";

export const CollectionPage = ({ match }) => {
    const collection = useSelector(state => selectShopCollection(match.params.collectionId)(state))
    const { items, title } = collection;

    return (
        <div className={"collection-page"}>
            <h2 className={"title"}>{title}</h2>
            <div className={"items"}>
                {items.map(item => <CollectionItem key={item.id} item={item} />)}
            </div>
        </div>
    )
}