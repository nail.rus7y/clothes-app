import React from "react";
import { WithSpinner } from "../../components/WithSpinner/WithSpinner";
import { CollectionPage } from "./CollectionPage";
import { useSelector } from "react-redux";
import { selectIsCollectionsLoaded } from "../../static/shopSelectors";

const CollectionPageWithSpinner = WithSpinner(CollectionPage);


export const CollectionPageContainer = (props) => {
    const isCollectionsLoaded = useSelector(selectIsCollectionsLoaded);
    return <CollectionPageWithSpinner isLoading={!isCollectionsLoaded} {...props} />
}