import React, { useEffect } from "react";
import { Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchCollectionsStartAsync } from "../../redux/shop/shopActions";
import { CollectionsOverviewContainer } from "../../components/CollectionsOverview/CollectionsOverviewContainer";
import { CollectionPageContainer } from "../CollectionPage/CollectionPageContainer";


export const ShopPage = ({ match }) => {
    const dispatch = useDispatch();
    useEffect(() => dispatch(fetchCollectionsStartAsync()), []);

    return (
        <div className={"shop-page"}>
            <Route exact path={`${match.path}`} component={CollectionsOverviewContainer} />
            <Route path={`${match.path}/:collectionId`} component={CollectionPageContainer} />
        </div>
    );
}