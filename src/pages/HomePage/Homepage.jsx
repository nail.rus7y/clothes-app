import React from "react";
import { Directory } from "../../components/Directory/Directory";
import { HomepagePageContainer } from "./Homepage.styles"

export const Homepage = () => {
    return (
      <HomepagePageContainer>
          <Directory />
      </HomepagePageContainer>
    );
}