import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import { persistRootReducer } from "./rootReducer";
import { persistStore } from "redux-persist";
import thunk from "redux-thunk";

const middlewares = [thunk];

if (process.env.NODE_ENV === "development") {
    middlewares.push(logger)
}

export const store = createStore(persistRootReducer, applyMiddleware(...middlewares))

export const persistor = persistStore(store);