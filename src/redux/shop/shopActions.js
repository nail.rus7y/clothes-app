import { FETCH_COLLECTIONS_START, FETCH_COLLECTIONS_FAILURE, FETCH_COLLECTIONS_SUCCESS } from "./shopActionTypes";
import { convertCollectionsSnapshotToMap, firestore } from "../../firebase/firebase";

export const fetchCollectionsStart = () => ({
    type: FETCH_COLLECTIONS_START,
})

export const fetchCollectionsSuccess = (collectionsMap) => ({
    type: FETCH_COLLECTIONS_SUCCESS,
    payload: collectionsMap,
})

export const fetchCollectionsFailure = (errorMessage) => ({
    type: FETCH_COLLECTIONS_FAILURE,
    payload: errorMessage,
})

export const fetchCollectionsStartAsync = () => {
    return dispatch => {
        const collectionRef = firestore.collection("collections");
        dispatch(fetchCollectionsStart());
        console.log("aga")
        collectionRef
            .get()
            .then(snapshot => {
                dispatch(fetchCollectionsSuccess(convertCollectionsSnapshotToMap(snapshot)));
            })
            .catch(error => dispatch(fetchCollectionsFailure(error.message)));
    }
}