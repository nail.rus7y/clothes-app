import React from "react";
import "./CollectionsOverview.styles.scss";
import { useSelector } from "react-redux";
import { selectCollectionsForPreview } from "../../static/shopSelectors";
import { CollectionPreview } from "../CollectionPreview/CollectionPreview";

export const CollectionsOverview = () => {
    const collections = useSelector(selectCollectionsForPreview);

    return (
        <div className={"collections-overview"}>
            {collections.map(({ id, ...collection }) =>
                <CollectionPreview key={id} { ...collection } />)}
        </div>
    )
}