import React from "react";
import { WithSpinner } from "../WithSpinner/WithSpinner";
import { CollectionsOverview } from "./CollectionsOverview";
import { useSelector } from "react-redux";
import { selectIsCollectionFetching } from "../../static/shopSelectors";

const CollectionOverviewWithSpinner = WithSpinner(CollectionsOverview);

export const CollectionsOverviewContainer = (props) => {
    const isFetching = useSelector(selectIsCollectionFetching);
    return <CollectionOverviewWithSpinner isLoading={isFetching} {...props} />
}