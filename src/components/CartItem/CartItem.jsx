import React from "react";
import "./CartItem.styles.scss";

export const CartItem = ({ item: { imageUrl, price, name, quantity } }) => {
    return (
        <div className={"cart-item"}>
            <img src={imageUrl} alt="item"/>
            <div className={"item-details"}>
                <span className={"name"}>{name}</span>
                <span className={"price"}>{price} * {quantity}</span>
            </div>
        </div>
    );
}