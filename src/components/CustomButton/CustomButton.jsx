import React from "react";
import { CustomButtonContainer } from "./CustomButton.styles.jsx";

export const CustomButton = ({ children, ...props }) => {
    return (
        <CustomButtonContainer { ...props }>
            {children}
        </CustomButtonContainer>
    )
}