import React, { useState } from "react";
import "./SignIn.styles.scss";
import { FormInput } from "../FormInput/FormInput";
import { CustomButton } from "../CustomButton/CustomButton";
import { auth, signInWithGoogle } from "../../firebase/firebase";

export const SignIn = () => {
    const [userInfo, setUserInfo] = useState({
        email: "",
        password: "",
    });

    async function handleSubmit(e) {
        e.preventDefault();

        const { password, email } = userInfo;

        try {
            await auth.signInWithEmailAndPassword(email, password);
            setUserInfo({ email: "", password: "" });
        } catch (e) {
            console.error(e);
        }
    }

    function handleChange(e) {
        const { value, name } = e.target;
        setUserInfo(prevState => ({ ...prevState, [name]: value }))
    }

    return (
        <div className={"sign-in"}>
            <h2>I already have an account</h2>
            <span>Sign in with your email and password</span>
            <form onSubmit={handleSubmit}>
                <FormInput label={"Email"} name="email" value={userInfo.email} required type="email" onChange={handleChange} />
                <FormInput label={"Password"} name="password" value={userInfo.password} required type="password" onChange={handleChange} />
                <div className={"buttons"}>
                    <CustomButton type="submit">Sign in</CustomButton>
                    <CustomButton onClick={signInWithGoogle} isGoogleSignIn>Sign in with Goggle</CustomButton>
                </div>
            </form>
        </div>
    )
}