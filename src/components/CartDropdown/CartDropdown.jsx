import React from "react";
import "./CartDropdown.styles.scss";
import { CustomButton } from "../CustomButton/CustomButton";
import { useDispatch, useSelector } from "react-redux";
import { CartItem } from "../CartItem/CartItem";
import { selectCartItems } from "../../redux/cart/cartSelectors";
import { useHistory } from "react-router-dom";
import { toggleCartHidden } from "../../redux/cart/cartActions";

export const CartDropdown = () => {
    const history = useHistory();
    const cartItems = useSelector(selectCartItems);
    const dispatch = useDispatch();

    return (
        <div className={"cart-dropdown"}>
            <div className={"cart-items"}>
                {cartItems.length > 0
                    ? cartItems.map(cartItem => <CartItem key={cartItem.id} item={cartItem}/>)
                    : <span className={"empty-message"}>Your cart is empty</span>
                }
            </div>
            <CustomButton onClick={() => {
                history.push("/checkout");
                dispatch(toggleCartHidden());
            }}>GO TO CHECKOUT</CustomButton>
        </div>
    )
}