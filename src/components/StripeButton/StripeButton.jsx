import React from "react";
import StripeCheckout from "react-stripe-checkout";

export const StripeButton = ({ price }) => {
    const priceForStripe = price * 100;
    const publishableKey = "pk_test_51IlfKoIwoRuKXIzPScpMQUMQzud858TSYNDFoxD5kAdlr6fpz01svrMHBhUfOdimScOLuee08VjUhGoMNJRhqamF00Iw3TJQfl";

    function onToken() {
        alert("Payment Successful");
    }

    return (
        <StripeCheckout
            label={"Pay Now"}
            name={"CRWN Clothing Ltd."}
            billingAddress
            shippingAddress
            image={"https://svgshare.com/i/Log.svg"}
            description={`Your total is $${price}`}
            amount={priceForStripe}
            panelLabel={"Pay Now"}
            token={onToken}
            stripeKey={publishableKey}
        />
    );
}