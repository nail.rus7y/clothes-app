import React from "react";
import { MenuItem } from "../MenuItem/MenuItem";
import "./Directory.styles.scss";
import { useSelector } from "react-redux";
import { selectDirectorySections } from "../../redux/directory/directorySelectors";

export const Directory = () => {
    const sections = useSelector(selectDirectorySections);

    return (
        <div className={"directory-menu"}>
            {sections.map(({ id, ...props }) =>
                <MenuItem key={id} {...props} />)}
        </div>
    )
}