import React, { useState } from "react";
import "./SignUp.styles.scss";
import { FormInput } from "../FormInput/FormInput";
import { CustomButton } from "../CustomButton/CustomButton";
import { auth, createUserProfileDocument } from "../../firebase/firebase";

export const SignUp = () => {
    const initialState = {
        displayName: "",
        email: "",
        password: "",
        confirmPassword: "",
    }
    const [userInfo, setUserInfo] = useState(initialState);
    const { email, displayName, password, confirmPassword } = userInfo;

    async function handleSubmit(e) {
        e.preventDefault();

        if (password !== confirmPassword) {
            alert("passwords do not match");
            return;
        }
        
        try {
            const { user } = await auth.createUserWithEmailAndPassword(email, password);
            console.log(displayName)
            await createUserProfileDocument(user, { displayName });
            setUserInfo(initialState);
        } catch (e) {
            console.error(e);
        }
    }

    function handleChange(e) {
        const { name, value } = e.target;
        setUserInfo(prevState => ({ ...prevState, [name]: value }));
    }

    return (
        <div className={"sign-up"}>
            <h2 className={"title"}>I do not have an account</h2>
            <span>Sign up with your email and your password</span>
            <form className={"sign-up-form"} onSubmit={handleSubmit}>
                <FormInput
                    type={"text"}
                    name={"displayName"}
                    value={displayName}
                    onChange={handleChange}
                    label={"Display Name"}
                    required
                />
                <FormInput
                    type={"email"}
                    name={"email"}
                    value={email}
                    onChange={handleChange}
                    label={"Email"}
                    required
                />
                <FormInput
                    type={"password"}
                    name={"password"}
                    value={password}
                    onChange={handleChange}
                    label={"Password"}
                    required
                />
                <FormInput
                    type={"password"}
                    name={"confirmPassword"}
                    value={confirmPassword}
                    onChange={handleChange}
                    label={"Confirm Password"}
                    required
                />
                <CustomButton type={"submit"}>Sign up</CustomButton>
            </form>
        </div>
    )
}