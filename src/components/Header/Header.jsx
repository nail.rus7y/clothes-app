import React from "react";
import { HeaderContainer, LogoContainer, OptionLink, OptionsContainer } from "./Header.styles.jsx";
import { ReactComponent as Logo } from "../../assets/crown.svg";
import { auth } from "../../firebase/firebase";
import { useSelector } from "react-redux";
import { CartIcon } from "../CartIcon/CartIcon";
import { CartDropdown } from "../CartDropdown/CartDropdown";
import { selectCartHidden } from "../../redux/cart/cartSelectors";
import { selectCurrentUser } from "../../redux/user/userSelectors";

export const Header = () => {
    const currentUser = useSelector(selectCurrentUser);
    const hidden = useSelector(selectCartHidden);

    return (
        <HeaderContainer>
            <LogoContainer to={"/"} >
                <Logo className={"logo"}/>
            </LogoContainer>
            <OptionsContainer>
                <OptionLink to={"/shop"}>
                    SHOP
                </OptionLink>
                <OptionLink to={"/shop"}>
                    CONTACT
                </OptionLink>
                {
                    currentUser
                        ? <OptionLink as={"div"} onClick={() => auth.signOut()}>SIGN OUT</OptionLink>
                        : <OptionLink to={"/signin"}>SIGN IN</OptionLink>
                }
                <CartIcon />
            </OptionsContainer>
            {hidden ? null : <CartDropdown />}
        </HeaderContainer>
    )
}